# PropertyStream #

### How to install? ###

[Nuget](https://www.nuget.org/packages/S.Utils/)

![ng.png](https://bitbucket.org/repo/76dnL7/images/2449394395-ng.png)


### INI Class Example ###

암호화를 사용하지않는 경우 **Initialization**메서드를 호출해줄 필요가 없습니다.
```
#!CSharp
    public class Setting : PropertyStream
    {
        [PropertyMethod(Default = "Who Am I?")]
        public string UserName
        {
            get
            {
                return GetValue<string>();
            }
            set
            {
                SetValue(value);
            }
        }

        [PropertyMethod(Comment = "This is age")]
        public int Age
        {
            get
            {
                return GetValue<int>();
            }
            set
            {
                SetValue(value);
            }
        }

        [PropertyMethod("Body", Default = 177.8)]
        public double Height
        {
            get
            {
                return GetValue<double>();
            }
            set
            {
                SetValue(value);
            }
        }

        public Setting(string path) : base(path, useCrypto:true)
        {
            this.AddCryptoChain(new Base64Crypto());
            this.Initialization();
        }
    }
```

### Crypto Chain Example ###
```
#!CSharp
    public class Base64Crypto : PropertyCrypto
    {
        public override byte[] OnDecrypt(byte[] data)
        {
            return Convert.FromBase64String(Encoding.Default.GetString(data));
        }
    
        public override byte[] OnEncrypt(byte[] data)
        {
            return Encoding.Default.GetBytes(Convert.ToBase64String(data));
        }
    }
```