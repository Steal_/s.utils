﻿using System;
using System.IO;
using System.Text;
using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;

using S.Utils.Security;
using S.Utils.Exceptions;

#if NET45
using System.Runtime.CompilerServices;
#endif

namespace S.Utils
{
    /// <summary>
    /// 클래스의 프로퍼티에대한 파일 저장 및 불러오기 메서드제공에 필요한
    /// 기본 기능을 구현해놓은 클래스이다.
    /// </summary>
    public abstract class PropertyStream : IDisposable
    {
        #region [ Constructor ]
        [Obsolete]
        private const string COMMENT_PATTERN = @"(@(?:""[^""]*"")+|""(?:[^""\n\\]+|\\.)*""|'(?:[^'\n\\]+|\\.)*')|//.*|/\*(?s:.*?)\*/";
        private const string COMMENT_PREFIX = "//";

        private const char CATEGORY_PREFIX = '[';
        private const char CATEGORY_SUFFIX = ']';

        private const char VALUE_DELIMITER = '=';
        private const char VALUE_PREFIX = '"';
        private const string VALUE_SUFFIX = @""";";

        // Environment.NewLine 너무 긺
        private const string NewLine = "\r\n";
        #endregion

        #region [ Property ]
        private bool _useCrypto;
        public bool UseCrypto
        {
            get
            {
                return _useCrypto;
            }
        }

        /// <summary>
        /// 파일의 이름을 가져옵니다.
        /// </summary>
        public string Name
        {
            get
            {
                return Path.GetFileName(fs.Name);
            }
        }

        /// <summary>
        /// 파일의 전체 경로를 가져옵니다.
        /// </summary>
        public string FullName
        {
            get
            {
                return Path.GetFullPath(fs.Name);
            }
        }

        public IPropertyCrypto[] CryptoChains
        {
            get
            {
                return _cryptoChains.ToArray();
            }
        }

        private bool _isLoaded = false;
        public bool IsLoaded
        {
            get
            {
                return _isLoaded;
            }
        }

        public bool IsRequireAdministrator { get; } = false;
        #endregion

        #region [ Local Variable ]
        private Encoding encoding;

        private FileStream fs;
        private ExMemoryStream buffer;
        private StreamWriter writer;
        private StreamReader reader;

        private Dictionary<string, PropertyData> props;
        private Dictionary<string, PropertyCategory> categories;
        private List<string> categorySet;

        private List<IPropertyCrypto> _cryptoChains;

        private string originalSource;
        #endregion

        #region [ Init ]
        /// <summary>
        /// 지정된 경로를 읽어와 S.Utils.PropertyStream 클래스의 새 인스턴스를 초기화합니다.
        /// </summary>
        /// <param name="path">현재 PropertyStream 개체가 캡슐화 할 파일에 대한 상대 또는 절대 경로입니다.</param>
        public PropertyStream(string path, Encoding encoding = null, bool useCrypto = false)
        {
            try
            {
                this.encoding = encoding ?? Encoding.Default;

                fs = new FileStream(path, FileMode.OpenOrCreate);
                
                _useCrypto = useCrypto;
                _cryptoChains = new List<IPropertyCrypto>();

                if (!useCrypto) Initialization();
            }
            catch (UnauthorizedAccessException)
            {
                IsRequireAdministrator = true;
                Console.WriteLine("추가 권한이 필요합니다.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw new Exception("처리되지 않은 예외가 발생했습니다.");
            }
        }

        ~PropertyStream()
        {
            props?.Clear();
            props = null;

            categories?.Clear();
            categories = null;

            fs?.Close();
            fs?.Dispose();
            fs = null;
        }
        #endregion

        #region [ Outer Func ]
        /// <summary>
        /// 프로퍼티 암호화 체인을 추가합니다.
        /// </summary>
        /// <param name="pc"></param>
        public void AddCryptoChain(IPropertyCrypto pc)
        {
            if (!_useCrypto) throw new UseCryptoDisabledException();
            if (_isLoaded) throw new AlreadyInitializedException();

            if (!_cryptoChains.Contains(pc))
            {
                _cryptoChains.Add(pc);
            }
        }

        /// <summary>
        /// 프로퍼티 암호화 체인을 제거합니다.
        /// </summary>
        /// <param name="pc"></param>
        public void RemoveCryptoChain(IPropertyCrypto pc)
        {
            if (!_useCrypto) throw new UseCryptoDisabledException();
            if (_isLoaded) throw new AlreadyInitializedException();

            if (_useCrypto && _cryptoChains.Contains(pc))
            {
                _cryptoChains.Remove(pc);
            }
        }

        /// <summary>
        /// 모든 설정을 기본값으로 재설정합니다.
        /// </summary>
        public void CleanUp()
        {
            foreach (var pd in props.Values)
            {
                CleanUp(pd.Name);
            }
        }

        /// <summary>
        /// 해당 설정을 기본값으로 재설정합니다.
        /// </summary>
        /// <param name="propertyName"></param>
        public void CleanUp(string propertyName)
        {
            propertyName = propertyName.ToLower();

            if (_isLoaded &&
                !string.IsNullOrEmpty(propertyName) &&
                props.ContainsKey(propertyName))
            {
                PropertyData pd = props[propertyName];

                _SetValue(propertyName, pd.Attribute.Default);
            }
        }

        /// <summary>
        /// 설정을 불러오기 전으로 돌립니다.
        /// </summary>
        public void RollBack()
        {
            GetStream().SetLength(originalSource.Length);
            GetStream().Seek(0, SeekOrigin.Begin);

            writer.Write(originalSource);
            Flush();

            _isLoaded = false;
            Initialization();
        }

        /// <summary>
        /// 모든 설정 저장합니다.
        /// </summary>
        public void SaveAll()
        {
            foreach (var pd in props.Values)
            {
                WriteValue(pd.Name);
            }
        }

        protected virtual void RemoveBlock(int position, int size)
        {
            Stream bs = GetStream();

            if (size <= 0) return;
            if (bs.Length < position)
                throw new Exception("스트림이 너무 짧습니다.");

            bs.Seek(position + size, SeekOrigin.Begin);

            int bufferSize = (int)(bs.Length - bs.Position);
            MemoryStream ms = new MemoryStream(bufferSize);
            bs.CopyTo(ms, bufferSize);

            bs.SetLength(bs.Length - size);

            byte[] buffer = ms.GetBuffer();
            bs.Seek(position, SeekOrigin.Begin);
            bs.Write(buffer, 0, buffer.Length);
        }

        protected virtual void InsertBlock(int position, int space)
        {
            Stream bs = GetStream();

            if (space <= 0) return;
            if (bs.Length < position)
                throw new Exception("스트림이 너무 짧습니다.");

            if (bs.Position != position)
            {
                bs.Seek(position, SeekOrigin.Begin);
            }

            int bufferSize = (int)(bs.Length - position);
            MemoryStream ms = new MemoryStream(bufferSize);
            bs.CopyTo(ms, bufferSize);
            
            // 공백 영역만큼 스트림 확장
            bs.SetLength(bs.Length + space);

            // 공백 덮어씀
            bs.Seek(position, SeekOrigin.Begin);
            if (space > 0) bs.Write(new byte[space], 0, space);

            // 버퍼 이어붙임
            byte[] buffer = ms.GetBuffer();
            bs.Seek(position + space, SeekOrigin.Begin);
            bs.Write(buffer, 0, buffer.Length);
        }


        protected virtual bool WriteValue(string propertyName)
        {
            if (_isLoaded &&
                !string.IsNullOrEmpty(propertyName) &&
                props.ContainsKey(propertyName.ToLower()))
            {
                PropertyData pd = props[propertyName.ToLower()];

                StringBuilder sb = new StringBuilder();
                ValuePacking(ref sb, pd);

                string value = sb.ToString();

                int vBitSize = BitSize(value);
                int delta = vBitSize - pd.ValueSize;

                if (delta > 0)
                {
                    InsertBlock(pd.Position, delta);
                }
                else
                {
                    RemoveBlock((int)(pd.Position + vBitSize), Math.Abs(delta));
                }

                GetStream().Seek(pd.Position, SeekOrigin.Begin);
                writer.Write(value);

                pd.ValueSize = vBitSize;
                PositionOffset(pd.Position, delta);

                Flush();
            }

            return false;
        }

#if NET45
        protected virtual void SetValue<T>(T value, [CallerMemberName] string propertyName = "")
        {
            _SetValue(propertyName, value);
        }

        protected virtual T GetValue<T>([CallerMemberName] string propertyName = "")
        {
            return _GetValue<T>(propertyName);
        }
#else
        protected virtual void SetValue<T>(string propertyName, T value)
        {
            _SetValue(propertyName, value);
        }

        protected virtual T GetValue<T>(string propertyName)
        {
            return _GetValue<T>(propertyName);
        }
#endif
        #endregion

        #region [ Inner Func ]
        private void Flush()
        {
            writer.Flush();
            OnEncode();
        }

        private void _SetValue<T>(string propertyName, T value)
        {
            propertyName = propertyName.ToLower();

            if (props.ContainsKey(propertyName))
            {
                props[propertyName].Value = value;

                WriteValue(propertyName);
            }
        }

        private T _GetValue<T>(string propertyName)
        {
            propertyName = propertyName.ToLower();

            if (props.ContainsKey(propertyName))
            {
                PropertyData pd = props[propertyName];
                
                return (T)(pd.Value ?? pd.Attribute.Default ?? default(T));
            }

            return default(T);
        }

        private int BitSize(string data)
        {
            return writer.Encoding.GetByteCount(data);
        }

        private bool IsAttachableCategory(string name)
        {
            foreach (var ctName in categorySet)
            {
                if (ctName.ToLower() == name.ToLower())
                {
                    return true;
                }
            }

            return false;
        }

        private string GetAttachableCategory(string name)
        {
            foreach (var ctName in categorySet)
            {
                if (ctName.ToLower() == name.ToLower())
                {
                    return ctName;
                }
            }

            return string.Empty;
        }

        private bool ValuePacking(ref StringBuilder buffer, PropertyData pd)
        {
            var value = pd.GetValue(this);
            bool multiline = (pd.Type == typeof(string) && value.Contains(NewLine));

            if (multiline) buffer.Append(VALUE_PREFIX);
            buffer.Append(value);
            if (multiline) buffer.Append(VALUE_SUFFIX);

            return multiline;
        }

        private object ValueParse(Type type, string value)
        {
            var converter = TypeDescriptor.GetConverter(type);

            if (converter != null)
            {
                try
                {
                    return converter.ConvertFromString(value);
                }
                catch
                { }
            }

            return Activator.CreateInstance(type);
        }

        private void PositionOffset(int standard, int offset)
        {
            props.Values.Where((pd) => pd.Position > standard)
                .ToList()
                .ForEach((pd) =>
                {
                    pd.Position += offset;
                });
        }
        #endregion

        #region [ Processing ]
        private Stream GetStream()
        {
            return ((Stream)buffer ?? fs);
        }

        private void OnDecode()
        {
            if (_useCrypto && _cryptoChains.Count > 0)
            {
                MemoryStream fMs = new MemoryStream();
                fs.CopyTo(fMs);

                byte[] data = new byte[fs.Length];
                Array.Copy(fMs.GetBuffer(), data, data.Length);

                for (int i = _cryptoChains.Count - 1; i >= 0; i--)
                {
                    try
                    {
                        data = _cryptoChains[i].OnDecrypt(data);
                    }
                    catch
                    {
                        data = new byte[0];
                        break;
                    }
                }

                if (data.Length > 0)
                {
                    buffer = new ExMemoryStream(data, 0, PropertyCrypto.GetLength(data), true, true);
                }
                else
                {
                    buffer = new ExMemoryStream(data, true, true);
                }

                writer = new StreamWriter(buffer, encoding) { AutoFlush = true };
                reader = new StreamReader(buffer, encoding);
            }
            else
            {
                writer = new StreamWriter(fs, encoding);
                reader = new StreamReader(fs, encoding);
            }
        }

        private void OnEncode()
        {
            if (_useCrypto && _cryptoChains.Count > 0)
            {
                byte[] data = buffer.GetBuffer();

                foreach (IPropertyCrypto pc in _cryptoChains)
                {
                    data = pc.OnEncrypt(data);
                }
                
                fs.SetLength(0);
                fs.Write(data, 0, PropertyCrypto.GetLength(data));
                fs.Flush();
            }
        }

        public void Initialization()
        {
            if (!_isLoaded)
            {
                OnDecode();

                props = new Dictionary<string, PropertyData>();
                categories = new Dictionary<string, PropertyCategory>();
                categorySet = new List<string>();

                ParsePropertyMethods();
                ParseLocalSource();
                SaveWithIndexing();
                //ParseMissingProperties();

                _isLoaded = true;
            }
        }

        private void ParsePropertyMethods()
        {
            if (props.Count > 0) return;
            
            var pMethods = this.GetType().GetProperties().
                Where((pi) => 
                {
                    var obj = pi.GetCustomAttributes(typeof(PropertyMethodAttribute), true);
                    return (obj.Length > 0);
                });

            foreach (var pi in pMethods)
            {
#if NET45
                if (pi.SetMethod == null)
                {
                    throw new Exception("PropertyMethod속성을 가진 프로퍼티는 set 메서드가 필요합니다.");
                }
#endif

                var propData = new PropertyData(pi);

                props.Add(pi.Name.ToLower(), propData);

                if (!categories.ContainsKey(propData.Category))
                {
                    PropertyCategory category = new PropertyCategory(propData.Category);
                    categories.Add(category.Name, category);

                    categorySet.Add(propData.Category);
                }

                categories[propData.Category].Values.Add(propData);
            }
        }

        private void ParseLocalSource()
        {
            GetStream().Seek(0, SeekOrigin.Begin);
            originalSource = reader.ReadToEnd();

            string[] lines = originalSource.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

            PropertyCategory currentCt = null;
            PropertyData currentData = null;

            bool isMultiline = false;
            StringBuilder valueBuffer = null;

            foreach (var line in lines)
            {
                var tmp = line.Trim();

                if (tmp.StartsWith(COMMENT_PREFIX)) continue;

                if ((tmp.Length > 0) &&
                    (tmp[0] == CATEGORY_PREFIX) &&
                    (tmp[tmp.Length - 1] == CATEGORY_SUFFIX))
                {
                    string ctName = tmp.Substring(1, tmp.Length - 2);

                    if (IsAttachableCategory(ctName))
                    {
                        ctName = GetAttachableCategory(ctName);
                        currentCt = categories[ctName];
                    }

                    continue;
                }

                if (currentCt == null) continue;

                if (!isMultiline)
                {
                    string[] lValues = line.Split(new char[] { VALUE_DELIMITER }, 2);

                    if (lValues.Length == 2)
                    {
                        string lcPropName = lValues[0].Trim().ToLower();
                        string lcValue = lValues[1].Trim();

                        if (props.ContainsKey(lcPropName))
                        {
                            currentData = props[lcPropName];
                        }
                        else
                        {
                            continue;
                        }

                        currentData.IsAttached = true;

                        if ((lcValue.Length > 0) &&
                            (lcValue[0] == VALUE_PREFIX) &&
                            (!lcValue.EndsWith(VALUE_SUFFIX)) &&
                            (Type.GetTypeCode(currentData.Type) == TypeCode.String))
                        {
                            isMultiline = true;
                            valueBuffer = new StringBuilder();

                            lcValue = lValues[1].TrimStart();
                            valueBuffer.AppendLine(lcValue.Substring(1, lcValue.Length - 1));

                            continue;
                        }
                        else if (lcValue.EndsWith(VALUE_SUFFIX))
                        {
                            lcValue = lValues[1].TrimStart();
                            lcValue = lcValue.Substring(1, lcValue.Length - VALUE_SUFFIX.Length - 1);
                        }

                        currentData.Value = ValueParse(currentData.Type, lcValue);
                    }
                }
                else
                {
                    if (currentData != null)
                    {
                        string lcValue = line;
                        string lcValueTE = lcValue.TrimEnd();

                        if (lcValueTE.EndsWith(VALUE_SUFFIX))
                        {
                            lcValue = lcValueTE.Substring(0, lcValueTE.Length - VALUE_SUFFIX.Length);
                            isMultiline = false;
                        }

                        if (!isMultiline)
                        {
                            valueBuffer.Append(lcValue);
                            currentData.Value = valueBuffer.ToString();
                        }
                        else
                        {
                            valueBuffer.AppendLine(lcValue);
                        }

                        continue;
                    }

                    isMultiline = false;
                }
            }
        }

        private void SaveWithIndexing()
        {
            GetStream().SetLength(0);

            long position = 0;
            Action<string> write = new Action<string>((s) =>
            {
                writer.Write(s);

                position += BitSize(s);
            });

            int nLineSize = BitSize(NewLine);
            foreach (var ct in categories.Values)
            {
                string ctHeader = $"{CATEGORY_PREFIX}{ct.Name}{CATEGORY_SUFFIX}{NewLine}";
                int bitSize = BitSize(ctHeader);

                if (position > 0)
                {
                    write(NewLine);
                }
                
                write(ctHeader);
                ct.Position = (int)position - bitSize + 1; // + 1 -> CATEGORY_PREFIX Size

                foreach (var pd in ct.Values)
                {
                    bool isMultiline = false;

                    if (!string.IsNullOrEmpty(pd.Attribute.Comment))
                    {
                        string cmtLine = $"{COMMENT_PREFIX} {pd.Attribute.Comment}{NewLine}";
                        write(cmtLine);
                    }

                    StringBuilder buffer = new StringBuilder();
                    buffer.Append(pd.Name);
                    buffer.Append($" {VALUE_DELIMITER} ");
                    isMultiline = ValuePacking(ref buffer, pd);
                    buffer.Append(NewLine);

                    string vLine = buffer.ToString();
                    int vnBitSz = BitSize(pd.Name);

                    pd.ValueSize = BitSize(pd.GetValue(this)) + (isMultiline ? 1 + VALUE_SUFFIX.Length : 0); // 1 + -> VALUE_PREFIX Size;
                    pd.Position = (int)position + vnBitSz + 3; // + 3 -> 2 Space + VALUE_DELIMITER Size
                    write(vLine);
                }
            }

            Flush();
        }

        [Obsolete]
        private void ParseMissingProperties()
        {
            foreach (var pd in props.Values)
            {
                if (!pd.IsAttached)
                {
                    //pd.IsAttached = true;
                    //pd.Value = pd.Attribute.Default;
                    //pd.ValueSize = BitSize(pd.GetValue(this));
                }
            }
        }

#endregion

#region [ Property Changed ]
#if NET45
        protected void OnPropertyChanged([CallerMemberName] string memberName = "")
        {
            WriteValue(memberName);
        }
#else
        protected void OnPropertyChanged(string memberName)
        {
            WriteValue(memberName);
        }
#endif
#endregion

#region [ IDisposable Support ]
        private bool isDisposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!isDisposed)
            {
                if (disposing)
                {
                    props?.Clear();
                    categories?.Clear();

                    fs?.Close();
                    fs?.Dispose();
                }

                props = null;
                categories = null;
                fs = null;

                isDisposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
#endregion
    }
}