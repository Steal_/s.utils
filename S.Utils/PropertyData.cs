﻿using System;
using System.Reflection;

namespace S.Utils
{
    internal class PropertyData : IStreamIndex
    {
        public PropertyMethodAttribute Attribute { get; }
        public PropertyInfo Property { get; }
        public string Category { get; }
        public string Name { get; }

        public int ValueSize { get; set; }
        public int Position { get; set; }

        public bool IsAttached { get; set; }
        public Type Type
        {
            get
            {
                return Property.PropertyType;
            }
        }

        public object Value { get; set; }
        
        public string GetValue(object owner)
        {
            return Property.GetValue(owner, null)?.ToString() ?? "";
        }

        public PropertyData(PropertyInfo pi)
        {
            this.Property = pi;
            this.Name = pi.Name;
#if NET45
            this.Attribute = pi.GetCustomAttribute<PropertyMethodAttribute>();
#else
            this.Attribute = (PropertyMethodAttribute)pi.GetCustomAttributes(typeof(PropertyMethodAttribute), true)[0];
#endif
            this.Category = Attribute.Category;
        }
    }
}
