﻿namespace S.Utils
{
    internal interface IStreamIndex
    {
        int Position { get; set; }
    }
}
