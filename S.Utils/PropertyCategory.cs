﻿using System;
using System.Collections.Generic;

namespace S.Utils
{
    internal class PropertyCategory : IStreamIndex
    {
        public string Name;
        public List<PropertyData> Values;

        public int Position { get; set; }

        public PropertyCategory(string name = "General")
        {
            Name = name;
            Values = new List<PropertyData>();
        }

    }
}
