﻿using System;
using System.IO;

namespace S.Utils
{
    internal class ExMemoryStream : Stream
    {
        private byte[] mBuffer;
        private bool mExposable;

        #region [ 생성자 ]
        public ExMemoryStream() : this(256)
        {
        }

        public ExMemoryStream(byte[] buffer)
        {
            mBuffer = buffer;
        }

        public ExMemoryStream(int capacity)
        {
            mBuffer = new byte[capacity];
            mExposable = true;
        }

        public ExMemoryStream(byte[] buffer, bool writable) : this(buffer)
        {
            _cWrite = writable;
            mExposable = false;
        }

        public ExMemoryStream(byte[] buffer, bool writable, bool publiclyVisible) : this(buffer)
        {
            _cWrite = writable;
            mExposable = publiclyVisible;
        }

        public ExMemoryStream(byte[] buffer, int index, int count) : this(count - index)
        {
            Array.Copy(buffer, mBuffer, mBuffer.Length);
        }

        public ExMemoryStream(byte[] buffer, int index, int count, bool writable) : this(buffer, index, count)
        {
            _cWrite = writable;
        }

        public ExMemoryStream(byte[] buffer, int index, int count, bool writable, bool publiclyVisible) : this(buffer, index, count, writable)
        {
            mExposable = writable;
            Array.Copy(buffer, mBuffer, mBuffer.Length);
        }
        #endregion

        public override bool CanRead
        {
            get
            {
                return true;
            }
        }

        public override bool CanSeek
        {
            get
            {
                return true;
            }
        }

        private bool _cWrite;
        public override bool CanWrite
        {
            get
            {
                return _cWrite;
            }
        }

        public override long Length
        {
            get
            {
                return mBuffer.Length;
            }
        }

        private long _position;
        public override long Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
            }
        }

        public override void Flush()
        {
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            int read = (int)Math.Min(count, (Length - Position));
            
            Array.Copy(mBuffer, Position, buffer, 0, read);
            Position += read;
            
            return read;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            switch (origin)
            {
                case SeekOrigin.Begin:
                    Position = offset;
                    break;

                case SeekOrigin.Current:
                    Position += offset;
                    break;

                case SeekOrigin.End:
                    Position = Length + offset;
                    break;
            }

            return Position;
        }

        public override void SetLength(long value)
        {
            byte[] data = new byte[value];

            if (Length < value)
            {
                Array.Copy(mBuffer, data, mBuffer.Length);
            }
            else if (Length > value)
            {
                Array.Copy(mBuffer, data, data.Length);
            }
            else
            {
                return;
            }

            if (Position > data.Length) Position = data.Length;
            mBuffer = data;
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            int dt = (int)(Length - (Position + count));
            
            if (dt < 0)
            {
                SetLength(Length - dt);
            }

            Array.Copy(buffer, offset, mBuffer, Position, count);
            Position += count;
        }

        public byte[] GetBuffer()
        {
            if (!mExposable)
                return null;

            return mBuffer;
        }
    }
}
