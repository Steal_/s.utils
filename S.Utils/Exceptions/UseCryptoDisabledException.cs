﻿using System;

namespace S.Utils.Exceptions
{
    public class UseCryptoDisabledException : Exception
    {
        private static string message = "프로퍼티 암호화 옵션이 비활성화돼있습니다.";

        public UseCryptoDisabledException() : base(message)
        {
        }
    }
}
