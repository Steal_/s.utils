﻿using System;

namespace S.Utils.Exceptions
{
    public class AlreadyInitializedException : Exception
    {
        private static string message = "이미 PropertyStream을 초기화 했습니다.";

        public AlreadyInitializedException() : base(message)
        {
        }
    }
}
