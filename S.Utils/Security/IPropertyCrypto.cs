﻿using System;
using System.Collections.Generic;
using System.IO;

namespace S.Utils.Security
{
    public interface IPropertyCrypto : IDisposable
    {
        byte[] OnEncrypt(byte[] data);
        byte[] OnDecrypt(byte[] data);
    }
}
