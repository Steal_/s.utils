﻿using System;
using System.IO;

namespace S.Utils.Security
{
    public abstract class PropertyCrypto : IPropertyCrypto
    {
        public virtual void Dispose()
        {
        }

        public abstract byte[] OnDecrypt(byte[] data);

        public abstract byte[] OnEncrypt(byte[] data);

        public static int GetLength(byte[] data)
        {
            for (int i = data.Length - 1; i >= 0; i--)
            {
                if (data[i] != 0)
                {
                    return i + 1;
                }
            }

            return -1;
        }
    }
}
