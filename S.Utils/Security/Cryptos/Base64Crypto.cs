﻿using System;
using System.Text;

namespace S.Utils.Security
{
    public class Base64Crypto : PropertyCrypto
    {
        public override byte[] OnDecrypt(byte[] data)
        {
            return Convert.FromBase64String(Encoding.Default.GetString(data));
        }

        public override byte[] OnEncrypt(byte[] data)
        {
            return Encoding.Default.GetBytes(Convert.ToBase64String(data));
        }
    }
}