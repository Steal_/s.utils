﻿using System;

namespace S.Utils
{
    /// <summary> 
    /// get, set메서드는 GetValue, SetValue 메서드로 정의하거나,
    /// <para>OnPropertyChanged 메서드를 이용해 프로퍼티를 정의해야됩니다.</para>
    /// </summary> 
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class PropertyMethodAttribute : Attribute
    {
        public string Category { get; }
        public string Comment { get; set; }
        public object Default { get; set; }

        public PropertyMethodAttribute(string category = "General")
        {
            Category = category;
        }
    }
}
