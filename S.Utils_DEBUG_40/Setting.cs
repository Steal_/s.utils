﻿using S.Utils;
using S.Utils.Security;

namespace S.Utils_DEBUG_40
{
    public class Setting : PropertyStream
    {
        [PropertyMethod(Default = "Who Am I?")]
        public string UserName
        {
            get
            {
                return GetValue<string>(nameof(UserName));
            }
            set
            {
                SetValue(nameof(UserName), value);
            }
        }

        [PropertyMethod(Comment = "나이")]
        public int Age
        {
            get
            {
                return GetValue<int>(nameof(Age));
            }
            set
            {
                SetValue(nameof(Age), value);
            }
        }

        [PropertyMethod("Body", Default = 177.8)]
        public double Height
        {
            get
            {
                return GetValue<double>(nameof(Height));
            }
            set
            {
                SetValue(nameof(Height), value);
            }
        }

        public Setting(string path) : base(path, useCrypto: true)
        {
            this.AddCryptoChain(new Base64Crypto());
            this.Initialization();
        }
    }
}
