﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace S.Utils_DEBUG_40
{
    class Program
    {
        static void Main(string[] args)
        {
            var s = new Setting("setting.ini");

            Console.WriteLine(s.UserName + ", " + s.Age);

            s.UserName = "Steal";
            s.Age = 19;
            Console.WriteLine(s.UserName + ", " + s.Age);

            s.CleanUp();
            Console.WriteLine(s.UserName + ", " + s.Age);

            s.UserName = "Hello Property Stream";

            s.Dispose();
        }
    }
}
