﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S.Utils_DEBUG_45
{
    class Program
    {
        static void Main(string[] args)
        {
            var s = new Setting("setting.ini");

            Console.WriteLine(s.UserName + ", " + s.Age);

            s.UserName = "Steal";
            s.Age = 19;
            Console.WriteLine(s.UserName + ", " + s.Age);

            s.CleanUp();
            Console.WriteLine(s.UserName + ", " + s.Age);

            s.UserName = "Hello Property Stream";

            s.Dispose();
        }
    }
}
