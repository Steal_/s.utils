﻿using S.Utils;
using S.Utils.Security;

namespace S.Utils_DEBUG_45
{
    public class Setting : PropertyStream
    {
        [PropertyMethod(Default = "Who Am I?")]
        public string UserName
        {
            get
            {
                return GetValue<string>();
            }
            set
            {
                SetValue(value);
            }
        }

        [PropertyMethod(Comment = "나이")]
        public int Age
        {
            get
            {
                return GetValue<int>();
            }
            set
            {
                SetValue(value);
            }
        }

        [PropertyMethod("Body", Default = 177.8)]
        public double Height
        {
            get
            {
                return GetValue<double>();
            }
            set
            {
                SetValue(value);
            }
        }

        public Setting(string path) : base(path, useCrypto: true)
        {
            this.AddCryptoChain(new Base64Crypto());
            this.Initialization();
        }
    }
}
